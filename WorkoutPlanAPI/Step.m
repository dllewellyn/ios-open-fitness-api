//
//  Step.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 28/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "Step.h"
#import "APIConstants.h"

@implementation Step

-(instancetype) initFromDictionary:(NSDictionary *) dict
{
    id me = nil;
    
    if (dict != nil) {
        me = [self init];
        self.intensity = [[Intensity alloc] initFromDictionary:dict[IntensityIdentifier]];
        self.stepDescription = dict[Description];
        self.timeInZone = dict[TimeInZone];
        self.orderOfStep = ((NSNumber*)dict[OrderOfStep]).integerValue;
    }
    return me;
}

@end
