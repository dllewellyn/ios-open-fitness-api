//
//  APIConstants.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>


// Workout List values

/* @brief Name of the workout */
FOUNDATION_EXPORT NSString *const WorkoutListName;

/* @brief Number of days */
FOUNDATION_EXPORT NSString *const DurationIdentifier;

/* @brief Workouts */
FOUNDATION_EXPORT NSString *const WorkoutsIdentifier;

// Workout values

/* @brief Name of the workout */
FOUNDATION_EXPORT NSString *const WorkoutName;

/* @brief Cooldown time */
FOUNDATION_EXPORT NSString *const CooldownIdentifier;

/* @brief Warmup time */
FOUNDATION_EXPORT NSString *const WarmupIdentifier;

/* @brief days into workout */
FOUNDATION_EXPORT NSString *const DaysIntoWorkout;

/* @brief overall intensity for workout */
FOUNDATION_EXPORT NSString *const OverallIntensity;

// Step list values

/* @brief value in json for step list */
FOUNDATION_EXPORT NSString *const StepListIdentifier;

/* @brief value in json for the name of the step list */
FOUNDATION_EXPORT NSString *const StepListName;

/* @brief value in json for the step list. */
FOUNDATION_EXPORT NSString *const StepIdentifier;

/* @brief Order of this step list in the workout */
FOUNDATION_EXPORT NSString *const StepListOrder;

/* Repetitions ID for step list */
FOUNDATION_EXPORT NSString *const RepetitionsIdentifier;

/* Ovearll time*/
FOUNDATION_EXPORT NSString *const OverallTime;

// Step values
/* @brief description of step */
FOUNDATION_EXPORT NSString *const Description;

/* @brief order of step */
FOUNDATION_EXPORT NSString *const OrderOfStep;

/* @brief time to spend on the step. */
FOUNDATION_EXPORT NSString *const TimeInZone;

/* @brief intensity of the workout */
FOUNDATION_EXPORT NSString *const IntensityIdentifier;

// Intensity values
/* @brief intensity description. */
FOUNDATION_EXPORT NSString *const IntensityDescriptionIdentifier;

/* @brief intensity name */
FOUNDATION_EXPORT NSString *const IntensityNameIdentifier;

// Workouts identifier from schedule
/* @brief name */
FOUNDATION_EXPORT NSString *const WorkoutIdentifier;

/* @brief dates identitifer */
FOUNDATION_EXPORT NSString *const DatesIdentifier;

/* @brief dates identitifer */
FOUNDATION_EXPORT NSString *const WorkoutListIdentifier;