//
//  DataRetrival.h
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataRetrieval : NSObject

// The callback that a user needs to provide us so that we can callback to them
typedef void (^ExternalCallback)(NSDictionary * _Nullable returnDict, NSError * _Nullable error);


/** Init with a URL
 @param url - url to use
 
 @return instance of DataRetrieval
 */
-(instancetype _Nonnull) initWithUrl:(NSURL* _Nonnull) url;

/** Init with a string that we will turn into a URL
 @param stringUrl - NSString of a URl
 
 @return instance of this object
 */
-(instancetype _Nonnull) initWithStringUrl:(NSString * _Nonnull) stringUrl;

// Url to get data from
@property  NSURL * _Nonnull url;

/**
  Retrieve data (which will be converted to a dictionary) from a remote URL
  Example usage:
        [retriever retrieveDataWithCallback:^(NSDictionary __nullable *returnDict, NSError __nullable *error) {
            if (returnDict != nil && error == nil) {
                // Do something with data
            }
        };
 */
-(void) retrieveDataWithCallback:(ExternalCallback _Nonnull) callback;

@end
