//
//  WorkoutList.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "WorkoutList.h"
#import "APIConstants.h"
#import "Workout.h"
#import "ErrorLogger.h"

@implementation WorkoutList

-(instancetype) initFromDictionary:(NSDictionary *) dict
{
    
    
    if (dict != nil) {
        id me = [self init];
        self.name = dict[WorkoutListName];
        self.duration = ((NSNumber*)dict[DurationIdentifier]).integerValue;
        
        NSArray *workoutsArray = dict[WorkoutsIdentifier];
        self.workouts = [self getWorkoutList:workoutsArray];
        return me;
    } else {
        return nil;
    }
    
    
}

/*
 @brief get a workout list given an array of workout objects 
 
 @param workoutArray an array of json objects that represent a workout
 
 @return An array of workout objects
 */
-(NSArray *) getWorkoutList:(NSArray*) workoutArray
{
 
    NSMutableArray<Workout *>*temp = [[NSMutableArray alloc] init];
    
    for (NSDictionary* object in workoutArray) {
            Workout *workout = [[Workout alloc] initFromDictionary:object];
            [temp addObject:workout];
    }
    
    return temp;
}



@end
