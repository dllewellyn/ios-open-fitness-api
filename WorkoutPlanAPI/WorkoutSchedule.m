//
//  WorkoutSchedule.m
//  WorkoutPlanAPI
//
//  Created by Daniel Llewellyn on 27/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import "APIConstants.h"
#import "WorkoutSchedule.h"
#import "WorkoutList.h"

@interface WorkoutSchedule()

// Dictionary to hold all the workouts
@property NSMutableDictionary<NSString *, Workout*> *workoutDictionary;

@property WorkoutList *workoutList;

// Dates
@property NSArray<NSString*>* dates;

// Keep this so we can save it if needed
@property NSDictionary *internalDictionary;

@end

@implementation WorkoutSchedule

-(instancetype _Nullable) initFromDictionary:(NSDictionary * _Nonnull) dict
{
    
    if (dict != nil) {
        
        self = [self init];
        NSDictionary *workoutDictionary = [dict objectForKey:WorkoutIdentifier];

        self.workoutDictionary = [[NSMutableDictionary alloc] init];
        self.dates = [dict objectForKey:DatesIdentifier];
        self.workoutList = [[WorkoutList alloc] initFromDictionary:[dict objectForKey:WorkoutListIdentifier]];
        self.internalDictionary = dict;

        for (NSString *localKey in self.dates) {
            NSDictionary *workoutDict = workoutDictionary[localKey];
            
            Workout *workout = nil;
            
            if ([workoutDict isKindOfClass:[NSString class]])
            {
                workout = [[Workout alloc] init];
            }
            else
            {
                workout = [[Workout alloc] initFromDictionary:workoutDict];
            }
            
            [self.workoutDictionary setObject:workout forKey:localKey];
        }
        
        return self;
    } else {
        return nil;
    }
    
}

-(long) getcount {
    
    long count = 0;
    
    if (self.workoutDictionary != nil)
    {
        count = [self.workoutDictionary count];
    }
    
    return count;
}

-(Workout*) getWorkout:(NSString *) date
{
    return self.workoutDictionary[date];
}

/**
 We'll save it to the same place every time and so know where to retrieve it also
 */
+(NSString *) getSavedPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"saved_file.plist"];
    return filePath;
}

/**
 Save the Dict to disk
 */
-(void) save {
    [self.internalDictionary writeToFile:WorkoutSchedule.getSavedPath atomically:YES];
}

+(WorkoutSchedule * _Nullable) retrieveWorkoutListFromDisk {
    NSDictionary *initWithMe = [NSDictionary dictionaryWithContentsOfFile:WorkoutSchedule.getSavedPath];
    return [[WorkoutSchedule alloc] initFromDictionary:initWithMe];
}

+(NSError *) deleteWorkoutListFromDisk
{
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:WorkoutSchedule.getSavedPath error:&error];
    
    return error;
}

@end
