//
//  Intensity.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 28/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "Intensity.h"
#import "APIConstants.h"

@implementation Intensity

-(instancetype) initFromDictionary:(NSDictionary *) dict
{
    id me = nil;
    
    if (dict != nil) {
        me = [self init];
        self.intensityDescription = dict[IntensityDescriptionIdentifier];
        self.intensityName = dict[IntensityNameIdentifier];
    }
    
    return me;
}

@end
