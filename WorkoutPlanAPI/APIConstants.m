//
//  APIConstants.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "APIConstants.h"
NSString *const WorkoutListName = @"name";
NSString *const DurationIdentifier = @"duration";
NSString *const WorkoutsIdentifier = @"workouts";


NSString *const WorkoutName = @"workout_name";
NSString *const CooldownIdentifier = @"cooldown";
NSString *const WarmupIdentifier = @"warm_up";
NSString *const DaysIntoWorkout = @"days_into_workout";
NSString *const OverallIntensity = @"overall_intensity";

NSString *const StepListIdentifier = @"step_lists";

NSString *const StepListName = @"name";
NSString *const StepIdentifier = @"step";
NSString *const StepListOrder = @"order";
NSString *const RepetitionsIdentifier = @"repetitions";
NSString *const OverallTime = @"total_time";

NSString *const Description = @"description";
NSString *const TimeInZone = @"time_in_zone";
NSString *const IntensityIdentifier = @"intensity";
NSString *const OrderOfStep = @"order_of_step";

NSString *const IntensityDescriptionIdentifier = @"intensity_description";
NSString *const IntensityNameIdentifier = @"intensity_name";

NSString *const WorkoutIdentifier = @"workouts";
NSString *const DatesIdentifier = @"dates";
NSString *const WorkoutListIdentifier = @"workout_list";