//
//  Settings.m
//  WorkoutPlanAPI
//
//  Created by Daniel Llewellyn on 27/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import "Settings.h"

@interface Settings()

// Base URL
@property  NSString *baseUrl;

// Market place url
@property NSString *marketPlaceHome;

// String to identify a json request
@property  NSString *jsonIdentifierString;

// String to request a schedule
@property  NSString *scheduleRequestBaseUrl;

@property NSString *queryUrl;

@end

@implementation Settings

static Settings *sharedMyManager = nil;


-(NSData *) getSettingsFile {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *filePath = [bundle pathForResource:@"settings" ofType:@"json"];

    NSData *data = [NSData dataWithContentsOfFile:filePath];
    return data;
}

-(instancetype) init {
    self = [super init];
    
    if (self != nil)
    {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self.getSettingsFile options:kNilOptions error:nil];
        self.baseUrl = [json objectForKey:@"BaseUrl"];
        self.jsonIdentifierString = [json objectForKey:@"JsonIdentifier"];
        self.scheduleRequestBaseUrl = [NSString stringWithFormat:@"%@/%@", self.baseUrl, [json objectForKey:@"ScheduleBaseUrl"]];
        self.marketPlaceHome = [NSString stringWithFormat:@"%@/%@", self.baseUrl, [json objectForKey:@"MarketPlaceHome"]];
        
        self.queryUrl = [NSString stringWithFormat:@"%@/%@", self.baseUrl, [json objectForKey:@"QueryBaseUrl"]];
        
        if (!self.baseUrl) {
            [NSException raise:@"A setting is nil" format:@"A setting is nil"];
        }
    }
    
    return self;
}

+ (instancetype) sharedManager {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{ // for thread safety
        sharedMyManager = [[Settings alloc] init];
    });
    
    return sharedMyManager;
}

-(NSString *) generateScheduleUrlRequestFor:(NSString *) workoutName andDate:(NSString *) date
{
    NSString *returnString = [NSString stringWithFormat:@"%@/%@/%@", self.scheduleRequestBaseUrl, workoutName, date];
    return returnString;
}

-(NSString *) generateUrlQueryRequestFor:(NSArray *) queries
{
    NSString *queriesString = @"";
    for (NSString *queryitem in queries)
    {
        if ([queriesString isEqualToString:@""])
        {
            queriesString = queryitem;
        }
        else
        {
            queriesString = [NSString stringWithFormat:@"%@,%@", queriesString, queryitem];
        }
    }
    
    NSString *returnString = [NSString stringWithFormat:@"%@/%@", self.queryUrl, queriesString];
    return returnString;
}
@end
