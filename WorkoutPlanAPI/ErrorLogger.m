//
//  ErrorLogger.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "ErrorLogger.h"

@implementation ErrorLogger

+(NSError*) logError:(NSString *) errorMessage
{
    if (errorMessage == nil) {
        errorMessage = @"Bloody blank message too";
    }
    
    NSLog(@"%@", errorMessage);
    
    NSString *domain = @"com.workoutgenerator.ErrorDomain";
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : errorMessage };
    
    NSError *error = [NSError errorWithDomain:domain
                                         code:-101
                                     userInfo:userInfo];
    return error;
    
}
@end
