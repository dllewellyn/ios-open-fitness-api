//
//  StepList.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 28/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "Step.h"

@interface StepList : NSObject

/*
 @brief Initialise from a dictionary. Can return nil if there's
 an error
 
 @param dict a Dictionary object. Please don't pass nil, if you pass nil,
 you'll get nil back as an ID
 
 @return a 'Workout' object. Or nil.
 */
-(instancetype) initFromDictionary:(NSDictionary *) dict;

// Name of the step list
@property NSString *name;

// Order of this step list in the workout
@property NSInteger order;

// Repetitions for this step list
@property NSInteger repetitions;

// A list of type Step
@property NSArray<Step *> *steps;

@end
