//
//  Step.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 28/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Intensity.h"

@interface Step : NSObject

/*
 @brief Initialise from a dictionary. Can return nil if there's
 an error
 
 @param dict a Dictionary object. Please don't pass nil, if you pass nil,
 you'll get nil back as an ID
 
 @return a 'Workout' object. Or nil.
 */
-(instancetype) initFromDictionary:(NSDictionary *) dict;

/* Time doing this step */
@property NSNumber *timeInZone;

/* Description of Step */
@property NSString *stepDescription;

/* When the step should be done */
@property NSInteger orderOfStep;

/** Intensity. */
@property Intensity *intensity;

@end
