//
//  DataRetrival.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import "DataRetrieval.h"
#import "ErrorLogger.h"

/**
 Class will be used to retrieve remote data from the URL provided as a constructor
 */
@implementation DataRetrieval

// Callback as deined for calling 'dataWithUrl'
typedef void (^CallBack)(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error);


// The callback that a user needs to provide us so that we can callback to them
typedef void (^ExternalCallback)(NSDictionary * __nullable returnDict, NSError * __nullable error);

-(instancetype) initWithUrl:(NSURL*) url
{
    if (url != nil)
    {
        self = [super init];
        self.url = url;
        
        return self;
    }
    else
    {
        return nil;
    }
}


-(void) retrieveDataWithCallback:(ExternalCallback) callback {
    
    // We make a local callback. In effect, this will take the response, see if there were
    // any errors, make a dictionary from the data and pass through to the 'callback' input
    // parameter.
    CallBack localCallback = ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        // Keep track of any error that occured
        NSError *localError;
        
        if (error != nil) {
            localError = error;
        }
        
        if (((NSHTTPURLResponse *)response).statusCode == 200) {
            if (!data) {
                localError = [ErrorLogger logError:[NSString stringWithFormat:@"Data is nil. Url is: %@", self.url]];
            }
        } else {
            localError = [ErrorLogger logError:[NSString stringWithFormat:@"Status code != 200. Url is: %@", self.url]];
        }
        
        NSDictionary *returnDict = nil;
        
        // Get the JSON data as a dict for returning to user
        if (data && localError == nil) {
            returnDict = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:kNilOptions
                                                                         error:&localError];
        }
        
        callback(returnDict, error);
        
    };
    
    
    // Make the request for data from a URL
    NSURLSession *aSession = [NSURLSession sessionWithConfiguration:
                              [NSURLSessionConfiguration defaultSessionConfiguration]];

    [[aSession dataTaskWithURL:self.url
             completionHandler:localCallback] resume];

}
-(instancetype) initWithStringUrl:(NSString *) stringUrl
{
    NSURL *url = [[NSURL alloc] initWithString:stringUrl];
    
    if (url == nil) {
        return nil;
    }

    return [self initWithUrl:url];
}
@end
