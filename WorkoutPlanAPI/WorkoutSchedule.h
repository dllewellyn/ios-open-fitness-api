//
//  WorkoutSchedule.h
//  WorkoutPlanAPI
//
//  Created by Daniel Llewellyn on 27/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Workout.h"
#import "WorkoutList.h"

@interface WorkoutSchedule : NSObject

/*
 @brief initialise from a dictionary in the correct format (e.g.
 {
 "name": ""
 } etc)
 
 @param dict dictionary conforming to above
 
 @return an instance of this class
 
 */
-(instancetype _Nullable) initFromDictionary:(NSDictionary * _Nonnull) dict;

@property (readonly, getter=getcount) long count;

// The list of dates
@property (readonly) NSArray<NSString*>  * _Nullable  dates ;

/* 
 Get a workout given the date
 
 @param date the date in the format 01-01-90 etc
 
 @return Workout workout for the date
 */
-(Workout* _Nullable) getWorkout:(NSString * _Nullable) date;

@property (readonly) NSMutableDictionary<NSString *, Workout*> * _Nullable workoutDictionary;

/**
 Workout list that this schedule has been generated for
 */
@property (readonly) WorkoutList * _Nullable workoutList;

/*
 @brief save a workout to disk
 */
-(void) save;

/**
 Retrieve a workout from disk
 
 @returns workoustList if stored on disk
 */
+(WorkoutSchedule * _Nullable) retrieveWorkoutListFromDisk;

/*
 Remove a workout list from disk
 @return NSError (will be NIL if everything was ok)
 */
+(NSError * _Nullable) deleteWorkoutListFromDisk;


@end
