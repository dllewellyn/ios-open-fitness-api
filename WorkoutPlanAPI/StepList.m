//
//  StepList.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 28/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "StepList.h"
#import "APIConstants.h"

@implementation StepList

-(instancetype) initFromDictionary:(NSDictionary *) dict
{
    id me = nil;
    
    if (dict != nil) {
        me = [self init];
        
        self.name = dict[StepListName];
        self.steps = [self getStepList:dict[StepIdentifier]];
        self.order = ((NSNumber*)dict[StepListOrder]).integerValue;
        self.repetitions = ((NSNumber*)dict[RepetitionsIdentifier]).integerValue;
    }
    
    return me;
}

/*
 @brief get a workout list given an array of workout objects
 
 @param stepList an array of json objects that represent a workout
 
 @return An array of Step objects
 */
-(NSArray *) getStepList:(NSArray*) stepList
{
    
    NSMutableArray<Step*> *temp = [[NSMutableArray alloc] init];

    for (NSDictionary* object in stepList) {
        Step *step = [[Step alloc] initFromDictionary:object];
        [temp addObject:step];
    }

    return temp;
}

@end
