//
//  Workout.m
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "Workout.h"
#import "APIConstants.h"

@implementation Workout

-(instancetype) init
{
    self = [super init];
    
    self.workoutName = @"Rest day";
    self.warmupTime = 0;
    self.cooldownTime = 0;
    self.stepLists = nil;
    self.daysIntoPlan = 0;
    self.overallIntensity = nil;
    self.overallDuration = 0;
    
    return self;
}

-(instancetype) initFromDictionary:(NSDictionary *) dict
{
    self = [super init];
    
    if (dict != nil) {
        self.workoutName = dict[WorkoutName];
        self.warmupTime = ((NSNumber*) dict[WarmupIdentifier]).integerValue;
        self.cooldownTime = ((NSNumber*) dict[CooldownIdentifier]).integerValue;
        self.stepLists = [self getStepLists:dict[StepListIdentifier]];
        self.daysIntoPlan = ((NSNumber*) dict[DaysIntoWorkout]).integerValue;
        self.overallIntensity = [[Intensity alloc] initFromDictionary:dict[OverallIntensity]];
        self.overallDuration = ((NSNumber*) dict[OverallTime]).integerValue;
    }
    
    return self;
}

/*
 @brief Get the step list as an NSArray
 
 @param stepLists a list of steps as a json array
 
 @return an array of step lists each step being of type step.
 */
-(NSArray *) getStepLists: (NSArray *) stepLists
{
    NSMutableArray<StepList *> *temp = [[NSMutableArray alloc] init];

    for (NSDictionary *dict in stepLists)
    {
        StepList *stepList = [[StepList alloc] initFromDictionary:dict];
        [temp addObject:stepList];
    }
    
    return temp;

}

@end
