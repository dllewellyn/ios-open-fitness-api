//
//  WorkoutList.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Workout.h"

@interface WorkoutList : NSObject

/* @brief Name of the workout plan */
@property NSString *name;

/* @brief Duration (in days) of the workout plan */
@property NSInteger duration;

/* @brief List of workouts */
@property NSArray<Workout*> *workouts;

/*
 @brief initialise from a dictionary in the correct format (e.g.
 {
    "name": ""
 } etc)
 
 @param dict dictionary conforming to above
 
 @return an instance of this class
 
 */
-(instancetype) initFromDictionary:(NSDictionary *) dict;


@end
