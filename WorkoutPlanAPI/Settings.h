//
//  Settings.h
//  WorkoutPlanAPI
//
//  Created by Daniel Llewellyn on 27/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

// Singleton getter
+ (instancetype) sharedManager;

// Base URL
@property (readonly) NSString *baseUrl;

// Market place url
@property (readonly) NSString *marketPlaceHome;

// String to identify a json request
@property (readonly) NSString *jsonIdentifierString;

// String to request a schedule
@property (readonly) NSString *scheduleRequestBaseUrl;

// Query URL
@property (readonly) NSString *queryUrl;


-(NSString *) generateScheduleUrlRequestFor:(NSString *) workoutName andDate:(NSString *) date;

-(NSString *) generateUrlQueryRequestFor:(NSArray *) queries;
@end
