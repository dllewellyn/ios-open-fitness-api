//
//  ErrorLogger.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorLogger : NSObject


/*
 @brief log an error to the system
 
 @param errorMessage An error message to log
 
 @return an error of NSError type
 */
+(NSError*) logError:(NSString *) errorMessage;

@end
