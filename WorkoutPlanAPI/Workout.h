//
//  Workout.h
//  JsonParserExample
//
//  Created by Daniel Llewellyn on 24/12/2015.
//  Copyright © 2015 Daniel Llewellyn. All rights reserved.
//

#import "StepList.h"

@interface Workout : NSObject

/*
 @brief Initialise from a dictionary. Can return nil if there's 
 an error
 
 @param dict a Dictionary object. Please don't pass nil, if you pass nil,
  you'll get nil back as an ID
 
 @return a 'Workout' object. Or nil.
 */
-(instancetype) initFromDictionary:(NSDictionary *) dict;

// Blank workout
-(instancetype) init;

/* @brief Name of workout */
@property NSString *workoutName;

/* @brief warmup time in seconds */
@property NSInteger warmupTime;

/* @brief cooldownTime in seconds */
@property NSInteger cooldownTime;

/* @brief The number of days into workout that we will do this workout. */
@property NSInteger daysIntoPlan;

/* @brief overall intensity */
@property Intensity *overallIntensity;

/* @brief overall duration */
@property NSInteger overallDuration;

/* @brief Step list*/
@property NSArray<StepList *> *stepLists;

@end
