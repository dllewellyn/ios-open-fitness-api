//
//  TestStepList.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Step.h"
#import "Intensity.h"
#import "APIConstants.h"
#import "StepList.h"

@interface TestStepList : XCTestCase

@end

@implementation TestStepList

StepList *stepList;

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValuesAreSetCorrectly {
    NSNumber *repetitions = @1;

    NSString *intensityName = @"Test intensity name";
    NSString *intensityDesc = @"Test intensity description";
    NSDictionary *dict = @{IntensityNameIdentifier: intensityName,
                          IntensityDescriptionIdentifier: intensityDesc
                           };
    
    NSString *stepDescription = @"Test step desc";
    NSNumber *timeInZone = @15;
    NSNumber *orderOfStep = @1;

    NSDictionary *step1Dict = @{IntensityIdentifier: dict,
                              TimeInZone: timeInZone,
                              OrderOfStep: orderOfStep,
                              Description: stepDescription};

    NSString *step2Description = @"Test step desc 2";
    NSNumber *timeInZone2 = @20;
    NSNumber *orderOfStep2 = @10;
    
    NSDictionary *step2Dict = @{IntensityIdentifier: dict,
                               TimeInZone: timeInZone2,
                               OrderOfStep: orderOfStep2,
                                Description: step2Description};
    
    NSString *stepListName = @"Step list";
    NSNumber *order = @1;
    
    NSArray *arrayOfSteps = @[step1Dict, step2Dict];
    
    NSDictionary *stepListDictionary = @{StepListName: stepListName,
                                        StepListOrder: order,
                                        StepIdentifier: arrayOfSteps,
                                         RepetitionsIdentifier: repetitions};
    
    stepList = [[StepList alloc] initFromDictionary:stepListDictionary];
    Step *step = (stepList.steps)[0];
    
    Step *step2 = (stepList.steps)[1];
    
    XCTAssertEqual(stepList.name, stepListName);
    XCTAssertEqual(stepList.order, [order integerValue]);
    XCTAssertEqual(stepList.repetitions, [repetitions integerValue]);
    
    Intensity *intensity = step.intensity;
    XCTAssertEqual(intensity.intensityDescription, intensityDesc);
    XCTAssertEqual(intensity.intensityName, intensityName);
    
    XCTAssertEqual(step.timeInZone, timeInZone);
    XCTAssertEqual(step.orderOfStep, [orderOfStep integerValue]);
    XCTAssertEqual(step.stepDescription, stepDescription);
    
    XCTAssertEqual(step2.timeInZone, timeInZone2);
    XCTAssertEqual(step2.orderOfStep, [orderOfStep2 integerValue]);
    XCTAssertEqual(step2.stepDescription, step2Description);
    
}

@end
