//
//  TestIntensity.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 07/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WorkoutList.h"

@interface TestWorkoutList : XCTestCase

@end

@implementation TestWorkoutList

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLoadJsonFromFile {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filePath = [bundle pathForResource:@"sample" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    WorkoutList *list = [[WorkoutList alloc] initFromDictionary:json[0]];
    
    XCTAssert([list.name isEqualToString:@"Basic workout"]);
    XCTAssert(list.duration == 14);
    
    Workout *workout = list.workouts[0];
    XCTAssert([workout.workoutName isEqualToString:@"Run"]);
    XCTAssert(workout.cooldownTime == 5);
    XCTAssert(workout.warmupTime == 10);
    XCTAssert(workout.overallDuration == 60);
    
    StepList *stepList = workout.stepLists[0];
    XCTAssert([stepList.name isEqualToString:@"Run"]);
    
    Step *step = stepList.steps[0];
    XCTAssert([step.stepDescription isEqualToString:@"Run at a constant pace"]);
    XCTAssert([step.timeInZone integerValue] == 45);
    
    Intensity *intensity = step.intensity;
    XCTAssert(intensity.intensityName = @"2");
    XCTAssert(intensity.intensityDescription = @"Endurance");
    
}


@end
