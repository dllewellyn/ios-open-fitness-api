//
//  TestStep.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Step.h"
#import "Intensity.h"
#import "APIConstants.h"

@interface TestStep : XCTestCase

@end

@implementation TestStep

Intensity *intensity;
Step *step;

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValuesAreSetCorrectly {
    NSString *intensityName = @"Test intensity name";
    NSString *intensityDesc = @"Test intensity description";
    NSDictionary *dict = @{IntensityNameIdentifier: intensityName,
                          IntensityDescriptionIdentifier: intensityDesc};
    
    NSString *stepDescription = @"Test step desc";
    NSNumber *timeInZone = @15;
    NSNumber *orderOfStep = @1;
    
    NSDictionary *stepDict = @{IntensityIdentifier: dict,
                              TimeInZone: timeInZone,
                              OrderOfStep: orderOfStep,
                              Description: stepDescription};
    
    step = [[Step alloc] initFromDictionary:stepDict];
    
    intensity = step.intensity;
    XCTAssertEqual(intensity.intensityDescription, intensityDesc);
    XCTAssertEqual(intensity.intensityName, intensityName);
    
    XCTAssertEqual(step.timeInZone, timeInZone);
    XCTAssertEqual(step.orderOfStep, [orderOfStep integerValue]);
    XCTAssertEqual(step.stepDescription, stepDescription);
    
}

@end
