//
//  TestDataRetrieval.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataRetrieval.h"
#import "WorkoutList.h"

@interface TestDataRetrieval : XCTestCase

@property DataRetrieval *retriever;

@end

@implementation TestDataRetrieval

- (void)setUp {
    [super setUp];
    self.retriever = [[DataRetrieval alloc] initWithStringUrl:@"http://dllewellyn.pythonanywhere.com/generator/lists"];
}

-(void) tearDown {
    [super tearDown];
}

- (void)testRetrievingDataFromUrl {
    XCTestExpectation *expectation = [self expectationWithDescription:@"High Expectations"];

    ExternalCallback callback = ^(NSDictionary * _Nullable returnDict, NSError * _Nullable error) {
        
        // Yeah... I know, but this method isn't really meant for retrieving all the lists
        // it's meant for getting one, but since we can't guarantee what its name will be...
        NSArray *arr = (NSArray *) returnDict;

        XCTAssertEqual(error, nil);
        XCTAssert(returnDict != nil);
        
        if (returnDict != nil && error == nil) {
            WorkoutList *workoutLists = [[WorkoutList alloc] initFromDictionary:arr[0]];
            XCTAssert(workoutLists.name != nil);
        }
        
        [expectation fulfill];
        
    };

    [self.retriever retrieveDataWithCallback:callback];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError *error) {
        if (error != nil) {XCTAssert(false);}
    }];
}

@end
