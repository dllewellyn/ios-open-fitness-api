//
//  TestIntensity.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Intensity.h"
#import "APIConstants.h"

@interface TestIntensity : XCTestCase

@end

@implementation TestIntensity {
    Intensity *intensity;
}
- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testValuesAreSetCorrectly {
    NSString *intensityName = @"Test intensity name";
    NSString *intensityDesc = @"Test intensity description";
    NSDictionary *dict = @{IntensityNameIdentifier: intensityName,
                          IntensityDescriptionIdentifier: intensityDesc};
    
    intensity = [[Intensity alloc] initFromDictionary:dict];
    XCTAssertEqual(intensity.intensityDescription, intensityDesc);
    XCTAssertEqual(intensity.intensityName, intensityName);
}


@end
