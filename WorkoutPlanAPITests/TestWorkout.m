//
//  TestWorkout.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 25/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Step.h"
#import "Intensity.h"
#import "APIConstants.h"
#import "StepList.h"
#import "Workout.h"

@interface TestWorkout : XCTestCase

@end

@implementation TestWorkout

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    [super tearDown];
}

- (void)testValuesAreSetCorrectly {
    
    NSString *intensityName = @"Test intensity name";
    NSString *intensityDesc = @"Test intensity description";
    NSDictionary *dict = @{IntensityNameIdentifier: intensityName,
                          IntensityDescriptionIdentifier: intensityDesc};
    
    NSString *stepDescription = @"Test step desc";
    NSNumber *timeInZone = @15;
    NSNumber *orderOfStep = @1;
    
    NSDictionary *step1Dict = @{IntensityIdentifier: dict,
                               TimeInZone: timeInZone,
                               OrderOfStep: orderOfStep,
                               Description: stepDescription};
    
    NSString *step2Description = @"Test step desc 2";
    NSNumber *timeInZone2 = @20;
    NSNumber *orderOfStep2 = @10;
    
    NSDictionary *step2Dict = @{IntensityIdentifier: dict,
                               TimeInZone: timeInZone2,
                               OrderOfStep: orderOfStep2,
                               Description: step2Description};
    
    NSString *stepListName = @"Step list";
    NSNumber *order = @1;
    
    NSArray *arrayOfSteps = @[step1Dict, step2Dict];
    
    NSDictionary *stepListDictionary = @{StepListName: stepListName,
                                        StepListOrder: order,
                                        StepIdentifier: arrayOfSteps};
    
    Workout *workout = [[Workout alloc] initFromDictionary:nil];

    NSString *workoutName = @"Test workout name";
    NSNumber *warmupTime = @15;
    NSNumber *cooldownTime = @10;
    NSNumber *daysIntoPlan = @10;
    
    NSDictionary *workoutDictionary = @{WorkoutName: workoutName,
                              WarmupIdentifier: warmupTime,
                              CooldownIdentifier: cooldownTime,
                              DaysIntoWorkout: daysIntoPlan,
                              OverallIntensity: dict,
                              StepListIdentifier: arrayOfSteps};
    workout = [[Workout alloc] initFromDictionary:workoutDictionary];
    
    XCTAssertEqual(workout.workoutName, workoutName);
    XCTAssertEqual([warmupTime integerValue], workout.warmupTime);
    XCTAssertEqual(workout.cooldownTime, [cooldownTime integerValue]);
    XCTAssertEqual(workout.daysIntoPlan, [daysIntoPlan integerValue]);
    
    StepList *stepList = workout.stepLists[0];
    stepList = [[StepList alloc] initFromDictionary:stepListDictionary];
    Step *step = (stepList.steps)[0];
    
    Step *step2 = (stepList.steps)[1];
    
    XCTAssertEqual(stepList.name, stepListName);
    XCTAssertEqual(stepList.order, [order integerValue]);
    Intensity *intensity = step.intensity;
    XCTAssertEqual(intensity.intensityDescription, intensityDesc);
    XCTAssertEqual(intensity.intensityName, intensityName);
    
    XCTAssertEqual(step.timeInZone, timeInZone);
    XCTAssertEqual(step.orderOfStep, [orderOfStep integerValue]);
    XCTAssertEqual(step.stepDescription, stepDescription);
    
    XCTAssertEqual(step2.timeInZone, timeInZone2);
    XCTAssertEqual(step2.orderOfStep, [orderOfStep2 integerValue]);
    XCTAssertEqual(step2.stepDescription, step2Description);
}

@end
