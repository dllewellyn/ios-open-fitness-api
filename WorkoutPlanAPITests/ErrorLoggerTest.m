//
//  ErrorLoggerTest.m
//  WorkoutPlans
//
//  Created by Daniel Llewellyn on 04/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ErrorLogger.h"
@interface ErrorLoggerTest : XCTestCase

@end

@implementation ErrorLoggerTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testErrorLoggingReturnsANonNilErrorMessage {
    NSError *error = nil;
    NSString *exampleError = @"test error message";
    
    error = [ErrorLogger logError:exampleError];
    XCTAssert([error userInfo][NSLocalizedDescriptionKey] == exampleError);
}

- (void)testErrorLoggingReturnsANilErrorMessage {
    NSError *error = nil;    
    error = [ErrorLogger logError:nil];
    XCTAssert([[error userInfo][NSLocalizedDescriptionKey] isEqualToString:@"Bloody blank message too"]);
}




@end
