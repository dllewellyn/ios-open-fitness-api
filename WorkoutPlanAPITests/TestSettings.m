//
//  TestSettings.m
//  WorkoutPlanAPI
//
//  Created by Daniel Llewellyn on 27/01/2016.
//  Copyright © 2016 Daniel Llewellyn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Settings.h"

@interface TestSettings : XCTestCase

@end

@implementation TestSettings

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    [super tearDown];
}

- (void)testGetAllSettings {
    Settings *settings = [Settings sharedManager];
    XCTAssert(settings.baseUrl != nil);
    XCTAssert([settings.marketPlaceHome isEqualToString:@"http://dllewellyn.pythonanywhere.com/market/home"]);
    XCTAssert([settings.jsonIdentifierString isEqualToString:@"?format=json"]);

}


@end
