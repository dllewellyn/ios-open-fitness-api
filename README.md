# README #

This workout API is designed to work with the Open Workout Plan API. For more info see

http://openfitnessapi.wordpress.com


# Basic usage #

Basic usage is:

     DataRetrieval *retriever = [[DataRetrieval alloc] initWithStringUrl:@"http://dllewellyn.pythonanywhere.com/generator/workout_list/Basic%20workout"];

    ExternalCallback callback = ^(NSDictionary * _Nullable returnDict, NSError * _Nullable error) {
        
        if (returnDict != nil && error == nil) {
            WorkoutList *workoutLists = [[WorkoutList alloc] returnDict];
			// Use it
		}
    };

    [self.retriever retrieveDataWithCallback:callback];